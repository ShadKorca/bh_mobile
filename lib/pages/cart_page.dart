import 'package:bh_mobile/models/Cart.dart';
import 'package:bh_mobile/widgets/cart_list_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CartPage extends StatelessWidget {
  const CartPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cartData = Provider.of<CartDataProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Basket of cocktaild'),
      ),
      body: cartData.cartItems.isEmpty
        ? Card(
          elevation: 5.0,
          margin: EdgeInsets.all(30.0),
          child: Container(
            height: 100,
            width: double.infinity,
            alignment: Alignment.center,
            child: Text('Your basked is empty'),
          ),
        )
        : Column(
          children: [
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(
                  'Costs: ' + cartData.totalAmount.toStringAsFixed(2),
                  style: TextStyle(fontSize: 20),
                ),
                MaterialButton(
                  onPressed: () {
                    cartData.clear();
                  },
                  color: Theme.of(context).primaryColor,
                  child: Text('Buy'),
                ),
              ],
            ),
            Divider(),

            Expanded(child: CartItemList(cartData: cartData)),
          ],
        )
    );
  }
}
