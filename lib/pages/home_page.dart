import 'package:bh_mobile/models/Product.dart';
import 'package:bh_mobile/widgets/bottom_bar.dart';
import 'package:bh_mobile/widgets/catalog.dart';
import 'package:bh_mobile/widgets/item_card.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final productData = Provider.of<ProductDataProvider>(context);

    return Scaffold(
      backgroundColor: Colors.amber,
      body: SafeArea(
          child: Container(
            height: MediaQuery.of(context).size.height - 85,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(35),
                bottomRight: Radius.circular(35),
              )
            ),
            child: ListView(
              padding: const EdgeInsets.all(10.0),
              children: <Widget>[
                Container(
                  child: ListTile(
                    title: Text(
                      'Fresh Cocktails',
                      style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text(
                      'More than 100 cocktails',
                      style: TextStyle(fontSize: 16),
                    ),
                    trailing: Icon(Icons.panorama_horizontal),
                  ),
                ),

                Container(
                  height: 290,
                  padding: const EdgeInsets.all(5.0),
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: productData.items.length,
                    itemBuilder: (context, int index) => 
                      ChangeNotifierProvider.value(
                        value: productData.items[index],
                        child: ItemCard(),
                      ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text('Catalog cocktails'),
                ),

                for(var el in productData.items) CatalogListTile(imgUrl: el.imgUrl,),

                // ...productData.items.map((e) => CatalogListTile(imgUrl:e.imgUrl)),
              ],
            ),
          ),
        ),
        bottomNavigationBar: BottomBar(),
        // !- Bottom bar
    );
  }
}
