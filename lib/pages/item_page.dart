import 'package:bh_mobile/models/Cart.dart';
import 'package:bh_mobile/models/Product.dart';
import 'package:bh_mobile/pages/cart_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class ItemPage extends StatelessWidget {
  final String productId;

  ItemPage({Key key, this.productId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final data = Provider.of<ProductDataProvider>(context).getElementById(productId);

    return Scaffold(
      appBar: AppBar(
        title: Text(data.title, style: GoogleFonts.marmelad()),
      ),
      body: Container(
        child: ListView(
          children: [
            Hero(
              tag: data.imgUrl,
              child: Container(
                height: 300,
                width: double.infinity,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(data.imgUrl),
                    fit: BoxFit.cover
                  )
                ),
              ),
            ),
            Card(
              elevation: 5.0,
              margin: EdgeInsets.symmetric(horizontal: 35, vertical: 10),
              child: Container(
                padding: EdgeInsets.all(30),
                child: Column(
                  children: [
                    Text(data.title, style: TextStyle(fontSize: 26)),
                    Divider(),
                    Row(
                      children: [
                        Text('Price: ', style: TextStyle(fontSize: 24)),
                        Text('${data.price}: ', style: TextStyle(fontSize: 24))
                      ],
                    ),
                    Divider(),
                    Text('${data.description}'),

                    SizedBox(
                      height: 20.0,
                    ),

                    Provider.of<CartDataProvider>(context).cartItems.containsKey(productId)
                    ? Column(
                      children: [
                        MaterialButton(
                          color: Color(0xFFCCFF90),
                          child: Text('Go to basket'),
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => CartPage()));
                            // Navigator.of(context).push(MaterialPageRoute(builder: (context) => CartPage()));
                          }
                        ),
                        Text(
                          'Product already added to basket',
                          style: TextStyle(
                            fontSize: 12.0,
                            color: Colors.blueGrey
                          ),
                        )
                      ],
                    )
                    : MaterialButton(
                      color: Theme.of(context).primaryColor,
                      child: Text('Add to backet'),
                      onPressed: () {
                        Provider.of<CartDataProvider>(context, listen: false).addItem(
                            productId: data.id,
                            price: data.price,
                            title: data.title,
                            imgUrl: data.imgUrl
                        );
                      },
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
