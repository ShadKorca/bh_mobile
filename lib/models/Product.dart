import 'dart:collection';

import 'package:flutter/foundation.dart';

class Product with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final num price;
  final String imgUrl;
  final color;

  Product({
    @required this.id,
    @required this.title,
    @required this.description,
    @required this.price,
    @required this.imgUrl,
    @required this.color
  });
}

class ProductDataProvider with ChangeNotifier {
  List<Product> _items = [
    Product(
        id: 'p3',
        title: 'Yellow cocktails',
        description: 'Hot cocktail for you',
        price: 15.00,
        imgUrl: 'https://www.acouplecooks.com/wp-content/uploads/2019/05/Gimlet-005.jpg',
        color: '0xFFFFF59D'
    ),
    Product(
        id: 'p1',
        title: 'Gin Gimlet',
        description: 'The gin gimlet is a classic cocktail made of lime juice and gin.',
        price: 35.0,
        imgUrl: 'https://images.squarespace-cdn.com/content/v1/5a5644aae45a7ca343e434a2/1556487323723-4GXGRZC4AMYQEGBPCNZ1/ke17ZwdGBToddI8pDm48kPOw4_NSiknRF4GcpcfUAYN7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1Uad1REWoJqlTVGoeXna77na4_E5zpNtbNzgwtzCEJCUJQADPtwErjFPsenxaex4sAg/pineapple+champagne+margarita+-+Amy+Traynor+-+moody+mixologist?format=original',
        color: '0xFFBBDEFB'
    ),
    Product(
        id: 'p2',
        title: 'Orange liquid',
        description: 'orange liquid in clear cocktail glass photo',
        price: 27.0,
        imgUrl: 'https://images.unsplash.com/photo-1587223962930-cb7f31384c19?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=934&q=80',
        color: '0xFABBAEFD'
    ),
  ];

  UnmodifiableListView<Product> get items => UnmodifiableListView(_items);

  Product getElementById(String id) => _items.singleWhere((element) => element.id == id);
}