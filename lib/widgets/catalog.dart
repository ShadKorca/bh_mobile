import 'package:bh_mobile/pages/catalog_page.dart';
import 'package:flutter/material.dart';

class CatalogListTile extends StatelessWidget {
  final imgUrl;

  CatalogListTile({Key key, this.imgUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) => ItemCatalog(imgUrl: imgUrl)));
      },
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: ListTile(
          leading: Container(
            width: 60,
            height: 60,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15.0),
                image: DecorationImage(
                    image: NetworkImage(imgUrl),
                    fit: BoxFit.cover
                )
            ),
          ),

          title: Text('Summer fresh catalog'),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('09:00 - 00:00'),
              Row(
                children: [
                  Icon(Icons.star, size: 15, color: Colors.amberAccent,),
                  Text('4.9'),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
